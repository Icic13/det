json.array!(@comments) do |comment|
  json.extract! comment, :id, :rails, :generate, :scaffold, :comment, :post_id, :body
  json.url comment_url(comment, format: :json)
end
